const request = require('request');

const helloWorld = (hello = false) => {
    if(hello) {
        return 'hello world';
    }
    return 'hola mundo';
};

const fetchGitHubApi = (done)=> {
    var options = {
        json: true,
        url: 'https://api.github.com/users/technoweenie',
        headers: {
          'User-Agent': 'request'
        }
      };
    request(options, function (error, response, body) {
        if (error) {
            return done(err);
        }
        return done(null, body);
    });
};

const fetchGitHubApiPm = ()=> {
    var options = {
        json: true,
        url: 'https://api.github.com/users/technoweenie',
        headers: {
          'User-Agent': 'request'
        }
      };
      return new Promise((resolve, reject)=>{
        request(options, function (error, response, body) {
            if(error) {
                return reject(error);
            }
            return resolve(body);
        });
      }); 
    
};

module.exports = {
    helloWorld,
    fetchGitHubApi,
    fetchGitHubApiPm
};
