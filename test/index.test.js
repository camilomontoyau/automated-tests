const mocha = require('mocha');
const {assert} = require('chai');
const {mensaje2} = require('../index');

describe('index.js', ()=>{
    it('mensaje2 debe retornar hello world 2', ()=>{
        assert.equal(mensaje2(), 'hello world 2');
    });
});
