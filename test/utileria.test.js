const mocha = require('mocha');
const {assert} = require('chai');
const {helloWorld, fetchGitHubApi, fetchGitHubApiPm} = require('../utileria');
const should = require('chai').should();

describe('utileria', ()=>{
    it('helloWorld debe retornar hola mundo', ()=>{
        assert.equal(helloWorld(), 'hola mundo');
    });
    it('fetchGitHubApi debe retornar un JSON', function(done){
        this.timeout(10000);
        fetchGitHubApi((err, resultado)=>{
            assert.isObject(resultado);
            assert.property(resultado, 'login');
            assert.notProperty(resultado, 'rrrrrrrrrr');
            done();
        });
    });
    it('fetchGitHubApiPm debe retornar un JSON', function(done){
        this.timeout(10000);
        fetchGitHubApiPm().then((resultado)=>{
            assert.isObject(resultado);
            assert.property(resultado, 'login');
            assert.notProperty(resultado, 'rrrrrrrrrr');
            done();
        });
    });
});
